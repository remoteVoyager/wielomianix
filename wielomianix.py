import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


class Wielomian:
	def __init__(self, wspol):
		self.wspol = wspol

	def __str__(self):
		text = "Wielomian: ([ "
		for x in range(len(self.wspol)):
			if x==len(self.wspol)-1:
				text+= str(self.wspol[x]) + " ])"
			else:
				text += str(self.wspol[x]) + ", "
		return text
	
	def __repr__(self):
		text = "Wielomian: ([ "
		for x in range(len(self.wspol)):
			if x==len(self.wspol)-1:	
			 text+= str(self.wspol[x]) + " ])"
			else:
				text += str(self.wspol[x]) + ", "
		return text

	def __call__(self, index):
		return self.wspol[index]
	
	def __len__(self):
		return len(self.wspol)
	
	def __add__(w1, w2):
		#w1 zawsze ma miec wiecej wspolczynnikow (dla uproszczenia reszty)
		if not len(w1)>=len(w2):
			w3 = w1
			w1,w2 = w2,w1
			w2 = w3
		
		#lista na wypadkowe wspolczynniki z sumy
		w3 = []

		i = 0
		#dla kazdego wspolczynnika dluzszego wielomianu
		for j in range(len(w1)):
			#print(w3)
			if(len(w1)-j>len(w2)):
				if w1(j)==0:
					continue
				else:
					w3.append(w1(j))
			else:
				w3.append(w1(j) + w2(i))
				i+=1

		return Wielomian(w3)

	def value_for_x(self, var_x):
		#zwraca wartosc wielomianu dla danego x
		result = 0

		power = len(self.wspol)-1

		for coef in self.wspol:
			result += coef * var_x**power
			power -= 1
		
		return result






def wykres_wielomianow(w1, w2, x_start, x_end):
	
	#takie cudo, glupie ale dziala
	#dx = abs(x_start-x_end)/(100*abs(x_start-x_end))
	dx=0.01

	values_1 = []
	values_2 = []
	sums = []
	x_args = []
	w3 = w1+w2

	#collect values
	while x_start <= x_end:
		y1 = w1.value_for_x(x_start)
		y2 = w2.value_for_x(x_start)
		values_1.append(y1)
		values_2.append(y2)
		sums.append(y1+y2)
		x_args.append(x_start)
		x_start += dx


	#display values
	plt.plot(x_args, values_1, label="w1")
	plt.plot(x_args, values_2, label="w2")
	plt.plot(x_args, sums, label="sum")


	plt.legend()
	plt.show()


# w1 = Wielomian([2,3,1])
# w2 = Wielomian([-5,2,4])

#testowe z zadania
w1 = Wielomian([-1/315, 32/315, -4/3, 416/45, -544/15, 3584/45, - 5632/63, 4096/105, 0])
w2 = Wielomian([1/20, 0, 0, 0, -2, 3])


# print(w1)
# print(w1(0))
# print(w2)
# print(w2(0))


# print(w1+w2)


wykres_wielomianow(w1, w2, -0.1, 3.2)

